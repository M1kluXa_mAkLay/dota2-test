from list_of_questions import question_dictionary
from time import sleep
import os
from random import sample


class Main:
    def __init__(self, username, right_unswers):
        self.username = username
        self.right_unswers = right_unswers
        

    def clear_console():
        os.system("cls")


    def run_menu(self, username):  
        print('Привет ' f'{username}, Сейчас ты пройдёшь тест на знание Dota2, на каждый вопрос у тебя будет по 10 секунд', '\n', 'всего будет 30 вопросов, также после прохождения тебе покажут время Удачи!')
        sleep(5)
        print("Начнём!")
    

    def game_prosses(self, right_unswers):
        random_keys = sample(tuple(question_dictionary.keys()), 30)
        for i in random_keys:
            print(*f'{question_dictionary[i][0]}?', *question_dictionary[i][1], sep = '\n')
            unser = input('Ваш ответ: ')
            try:
                if not unser in question_dictionary[i][1]:
                    raise Exception
            except:
                print("Некорректный ввод. Вы уверены, что ввели один из варинантов ответа?")
                continue
            if unser == question_dictionary[i][2]:
                right_unswers = right_unswers + 1
            else:
                right_unswers = right_unswers + 0
                
        return right_unswers

    def evaluation_criteria(self, right_unswers, user_name):
        if right_unswers >= 0 and right_unswers <= 6:
            print('Совсем не играл в Dota 2 :(')
        elif right_unswers > 6 and right_unswers < 11:
            print('посмотрел 2 гайда.')
        elif right_unswers >= 11 and right_unswers <= 16:
            print('Хорошо знаешь, но не всё')
        elif right_unswers > 16 and right_unswers < 20:
            print(f'{user_name} - Настоящий ФАНАТ ДОТЫ :)')



         
user = Main(0, 0)

user.run_menu(input('Введите имя: '))
user.game_prosses()
user.evaluation_criteria()